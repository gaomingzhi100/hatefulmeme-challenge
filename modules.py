import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models
from functools import partial

class ScaledDotProductAttention(nn.Module):
    ''' Scaled Dot-Product Attention '''
    def __init__(self, temperature, attn_dropout=0.1):
        super().__init__()
        self.temperature = temperature
        self.dropout = nn.Dropout(attn_dropout)
        self.softmax = nn.Softmax(dim=2)

    def forward(self, q, k, v, mask=None):
        # as the data type of our mask is FloatTensor, we need to convert to bool type by (mask == 0.0)
        attn = torch.bmm(q, k.transpose(1, 2))
        attn = attn / self.temperature

        # attn.shape: torch.Size([batch_size * n_head, 1, seq_len])
        if mask is not None:
            length = k.size(1)
            mask = mask[:,:, :length]

            attn = attn.masked_fill(mask == 0.0, float('-inf'))

        attn = self.softmax(attn)
        attn = self.dropout(attn)
        output = torch.bmm(attn, v)

        return output, attn
    
class MultiHeadAttention(nn.Module):
    def __init__(self, n_head, d_model, d_k, d_v, dropout=0.1, is_regu=False):
        super().__init__()

        self.n_head = n_head
        self.d_k = d_k
        self.d_v = d_v
        self.is_regu = is_regu

        self.w_qs = nn.Linear(d_model, n_head * d_k)
        self.w_ks = nn.Linear(d_model, n_head * d_k)
        self.w_vs = nn.Linear(d_model, n_head * d_v)
        nn.init.normal_(self.w_qs.weight, mean=0, std=np.sqrt(2.0 / (d_model + d_k)))
        nn.init.normal_(self.w_ks.weight, mean=0, std=np.sqrt(2.0 / (d_model + d_k)))
        nn.init.normal_(self.w_vs.weight, mean=0, std=np.sqrt(2.0 / (d_model + d_v)))

        self.attention = ScaledDotProductAttention(temperature=np.power(d_k, 0.5))
        self.layer_norm = LayerNorm(d_model)

        self.fc = nn.Linear(n_head * d_v, d_model)
        nn.init.xavier_normal_(self.fc.weight)
        self.dropout = nn.Dropout(dropout)

    def diff_outputs(self, inputs):
        # inputs:  # batch_size * src_len * n_head * dim (src_len is 1), e.g., torch.Size([64, 1, 4, 256])
        x = F.normalize(inputs, p=2, dim=-1)
        assert x.size(1) == 1, 'in our work, the sequence len for the query is only 1'
        x1 = x.squeeze(1)  # [bs, n_head, dim]
        x2 = x1.permute(0, 2, 1)  # [bs, dim, n_head]
        cos_diff = torch.bmm(x1, x2)  # [bs, n_head, n_head]
        cos_diff_square = cos_diff ** 2
        # Yue: no need to exclude the diagonal elements, as its square is always 1
        # cos_diff_square_mean = torch.mean(cos_diff_square, dim=[1, 2])
        n_head = inputs.size(2)
        idx = torch.arange(0, n_head)
        cos_diff_square[:, idx, idx] = 0
        cross_head_num = n_head * (n_head - 1)
        cos_diff_square_mean = torch.sum(cos_diff_square, dim=[1, 2]).div_(cross_head_num)
        return cos_diff_square_mean

    def forward(self, q, k, v, mask=None):
        '''
        q: [batch, 1, hidden_size]
        k, v: [batch, num_entry, hidden_size]
        '''
        d_k, d_v, n_head = self.d_k, self.d_v, self.n_head

        sz_b, len_q, _ = q.size()
        sz_b, len_k, _ = k.size()
        sz_b, len_v, _ = v.size()

        residual = q

        q = self.w_qs(q).view(sz_b, len_q, n_head, d_k)
        k = self.w_ks(k).view(sz_b, len_k, n_head, d_k)
        v = self.w_vs(v).view(sz_b, len_v, n_head, d_v)

        q = q.permute(2, 0, 1, 3).contiguous().view(-1, len_q, d_k)  # (n*b) x lq x dk
        k = k.permute(2, 0, 1, 3).contiguous().view(-1, len_k, d_k)  # (n*b) x lk x dk
        v = v.permute(2, 0, 1, 3).contiguous().view(-1, len_v, d_v)  # (n*b) x lv x dv

        if mask is not None:
            mask = mask.repeat(n_head, 1, 1)  # (n*b) x .. x ..
        output, attn = self.attention(q, k, v, mask=mask)

        output = output.view(n_head, sz_b, len_q, d_v)

        output = output.permute(1, 2, 0, 3).contiguous()  # b x lq x n * dv

        if self.is_regu:
            regu_term = self.diff_outputs(output)

        output = output.view(sz_b, len_q, -1)  # b x lq x (n*dv)

        output = self.dropout(self.fc(output))
        output = self.layer_norm(output + residual)

        if self.is_regu:
            return output, attn, regu_term
        return output, attn

class PositionwiseFeedForward(nn.Module):
    ''' A two-feed-forward-layer module '''

    def __init__(self, d_in, d_hid, dropout=0.1):
        super().__init__()
        self.w_1 = nn.Conv1d(d_in, d_hid, 1)  # position-wise
        self.w_2 = nn.Conv1d(d_hid, d_in, 1)  # position-wise
        self.layer_norm = LayerNorm(d_in)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x):
        residual = x
        output = x.transpose(1, 2)
        output = self.w_2(F.relu(self.w_1(output)))
        output = output.transpose(1, 2)
        output = self.dropout(output)
        output = self.layer_norm(output + residual)
        return output

class LayerNorm(nn.Module):
    """ 
    Layer Normalization 
    """
    def __init__(self, features, eps=1e-6):
        super().__init__()
        self.gamma = nn.Parameter(torch.ones(features))
        self.beta = nn.Parameter(torch.zeros(features))
        self.eps = eps

    def forward(self, x):
        mean = x.mean(-1, keepdim=True)
        std = x.std(-1, keepdim=True)
        return self.gamma * (x - mean) / (std + self.eps) + self.beta

def masked_mean(input, mask=None, dim=1):
    # 
    # input: [batch_size, seq_len, hidden_size]
    # mask: Float Tensor of size [batch_size, seq_len], where 1.0 for unmask, 0.0 for mask ones
    if mask is None:
        return torch.mean(input, dim=dim)
    else:
        length = input.size(1)
        mask = mask[:,:length].unsqueeze(-1)
        mask_input = input * mask
        sum_mask_input = mask_input.sum(dim=dim)
        mask_ = mask.sum(dim=dim)
        sum_mask_out = sum_mask_input/mask_
        return sum_mask_out
    
class MaskedSoftmax(nn.Module):
    def __init__(self, dim):
        super(MaskedSoftmax, self).__init__()
        self.dim = dim

    def forward(self, logit, mask=None):
        if mask is None:
            dist = F.softmax(logit - torch.max(logit, dim=self.dim, keepdim=True)[0], dim=self.dim)
        else:
            dist_ = F.softmax(logit - torch.max(logit, dim=self.dim, keepdim=True)[0], dim=self.dim) * mask
            normalization_factor = dist_.sum(self.dim, keepdim=True)
            dist = dist_ / normalization_factor
        return dist
    

class MyMultiHeadAttention(nn.Module):
    def __init__(self, n_head, d_model, d_kv, dropout=0.1, need_mask=False, is_regu=False):
        super(MyMultiHeadAttention, self).__init__()
        self.need_mask = need_mask
        self.is_regu = is_regu
        self.slf_attn = MultiHeadAttention(n_head, d_model, d_kv, d_kv, dropout=dropout, is_regu=is_regu)
        self.pos_ffn = PositionwiseFeedForward(d_model, d_model, dropout=dropout)

    def forward(self, q, k, v, mask=None):
        # q: [batch_size, d_model] ==>  k: [batch_size, 1, d_model]
        # mask: [batch_size, seq_len] == > [batch_size, 1, seq_len]
        # when there is only one query, we need to expand the dimension
        if len(q.shape) == 2:
            q = q.unsqueeze(1)
        if mask is not None:
            mask = mask.unsqueeze(1)  # [batch_size, 1, seq_len]
        if self.need_mask:
            assert mask is not None, 'Please pass the attention mask to the multi-head'

        if self.is_regu:
            enc_output, enc_slf_attn, head_diff = self.slf_attn(q, k, v, mask)
        else:
            # print(self.slf_attn(q, k, v, mask))
            enc_output, enc_slf_attn = self.slf_attn(q, k, v, mask)
        enc_output = self.pos_ffn(enc_output)

        # enc_output: [batch_size, 1, d_model] ==>  k: [batch_size, d_model]
        enc_output = enc_output.squeeze(1)
        if self.is_regu:
            return enc_output, enc_slf_attn, head_diff
        return enc_output, enc_slf_attn
