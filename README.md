# Hatefulmeme challenge

## Motivation

To comprehensively understand image-text pairs, different levels of feature alignment are mutually reinforcing. Images contain multi-level semantic features and Both high-level image contextual semantics and low-level image pattern information could prompt cross-modal alignment and interaction with abstract semantics in texts to obtain more key and comprehensive clues, which is necessary for effective multimodal reasoning in detecting hateful memes

## Model Framework

![](model_framework.jpg)


