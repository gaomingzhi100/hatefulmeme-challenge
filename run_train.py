import os

import opts
import torch
import utils
from train import train

os.environ["CUDA_VISIBLE_DEVICES"] = "0"


if __name__ == "__main__":

    opt = opts.parse_opt()
    opt.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    
    utils.setup_seed(opt.seed)
    train(opt)