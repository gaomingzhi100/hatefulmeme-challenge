from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pandas as pd
from tqdm import tqdm
import os
import numpy as np
import torch
import torch.nn.functional as F
from model import MultimodalEncoder
from dataset import HatefulMemesDataset
from torch.utils.data import DataLoader
from sklearn import metrics
import opts

def eval(opt):
    # save prediction results
    # results = pd.DataFrame(
    #     index=eval_dataset.samples_frame.id,
    #     columns=["proba", "label"]
    # ) 
    eval_dataset = HatefulMemesDataset(opt.dev_unseen_path)
    eval_dataloader = DataLoader(eval_dataset, shuffle=False, batch_size=opt.train_batch_size, num_workers=opt.num_workers)
    
    model = MultimodalEncoder(opt).to(opt.device)
    model.eval()

    # load trained model
    model_save_path = os.path.join(opt.save_path, 'model_best.pth')
    model.load_state_dict(torch.load(model_save_path))
    model.to(opt.device)

    predictions, true_labels = [], []

    for _, batch in enumerate(tqdm(eval_dataloader, desc="Iteration")):

        image, texts, labels, targets = batch

        image = image.to(opt.device)
        labels = labels.to(opt.device)
        targets = targets.to(opt.device)

        with torch.no_grad():
            logits = model(image, texts)
            preds = F.softmax(logits,dim=1).argmax(dim=1)

        # results.loc[idx, "proba"] = preds
        # results.loc[idx, "label"] = logits.argmax(dim=1)

        predictions.append(preds.detach().cpu().numpy())
        true_labels.extend(labels.to('cpu').numpy())

    predict_all = np.vstack(predictions)
    label_all = np.stack(true_labels)

    eval_acc = metrics.accuracy_score(predict_all, label_all)
    eval_auc = metrics.roc_auc_score(predict_all, label_all)

    print()
        # logging.info("Epoch | Train Accuracy  | label_0 F1  | label_1 F1  | label_2 F1  | label_3 F1  | label_4 F1  | Validation F1  | Training Loss | Validation Loss")
        # logging.info(
        #     f"{train_idx + 1:4d} |  {train_acc:.4f}  |    {f_score[0]:.4f}     |    {f_score[1]:.4f}      |    {f_score[2]:.4f}     |    {f_score[3]:.4f}    |    {f_score[4]:.4f}    |   {val_f1:.4f}      |    {avg_train_loss:.4f}    |     {avg_val_loss:.4f}")
    print(
        "eval accuracy | eval auc score")
    print(
        f"{eval_acc:4d} |  {eval_auc:.4f}  ")
    
    # results.proba = results.proba.astype(float)
    # results.label = results.label.astype(int)
    return eval_acc, eval_auc
    
if __name__ == "__main__":
    opt = opts.parse_opt()
    opt.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    eval(opt)
