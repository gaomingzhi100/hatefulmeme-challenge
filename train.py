from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn
import numpy as np
import os
from model import MultimodalEncoder
from dataset import HatefulMemesDataset
from torch.utils.data import DataLoader
from tqdm import tqdm, trange
import pytorch_warmup as warmup
import torch.nn.functional as F
from sklearn import metrics
os.environ["TOKENIZERS_PARALLELISM"] = "false"

def train(opt):
    # logging.info("***********************************************************************************************")
    # logging.info("run experiments with seed {}, {}".format(opt.seed, opt.second_mode))
    train_dataset = HatefulMemesDataset(opt.train_path,opt.img_path)
    dev_dataset = HatefulMemesDataset(opt.dev_path, opt.img_path)
    train_dataloader = DataLoader(train_dataset, shuffle=True, batch_size=opt.train_batch_size, num_workers=0)
    dev_dataloader = DataLoader(dev_dataset, shuffle=False, batch_size=opt.train_batch_size, num_workers=0)
    
    model = MultimodalEncoder(opt).to(opt.device)

    crit = nn.functional.binary_cross_entropy_with_logits

    # filter tunable parameters
    tunable_layers = ['layer.10','layer.11','pooler','classifer','classifier','proj','text2img','text2vgg']
    params = {}
    for name, param in model.named_parameters():
        for ele in tunable_layers:
            if ele in name:
                params[name] = param
                break
                
    # define optimizer
    no_decay = ["bias", "LayerNorm.weight"]
    optimizer_grouped_parameters = [
            {
                "params": [p for n, p in params.items() if not any(nd in n for nd in no_decay)],
                "weight_decay": 0.01,
            },
            {
                "params": [p for n, p in params.items() if any(nd in n for nd in no_decay)],
                "weight_decay": 0.0,
            },
        ]
    optimizer = torch.optim.AdamW(optimizer_grouped_parameters,
#                                   model.parameters(),
                                  lr=opt.learning_rate)
#                                   betas=(0.9, 0.999), 
#                                   warmup=opt.warmup_proportion)
#                                   weight_decay=0.01)

    num_steps = int(len(train_dataloader) * opt.max_epochs * 1.1)
    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, T_max=num_steps)
    warmup_scheduler = warmup.UntunedLinearWarmup(optimizer)

    # logging.info("***** Running training *****")
    print("***** Running training *****")
    best_val_score = 0
    for epoch in trange(int(opt.max_epochs), desc="Epoch"):
        # logging.info("********** Epoch: " + str(train_idx) + " **********")
        # logging.info("  Num examples = %d", len(train_dataset))
        # logging.info("  Batch size = %d", opt.train_batch_size)
        # logging.info("  Num steps = %d", num_train_optimization_steps)

        model.train()
        total_train_loss, total_train_len = 0, 0
        predictions, true_labels = [], []

        for _, batch in enumerate(tqdm(train_dataloader)):

            image, texts, labels, targets = batch
            total_train_len += image.shape[0]
            image = image.to(opt.device)
            labels = labels.to(opt.device)
            targets = targets.to(opt.device)
            
            with torch.autograd.set_detect_anomaly(True):
                logits = model(image, texts)

                loss = crit(logits, targets)
                loss.backward()

            optimizer.step()
            optimizer.zero_grad()

            total_train_loss += loss.item()
            # pred = predict_out.argmax(1, keepdim=True).float()
            # correct_tensor = pred.eq(labels.float().view_as(pred))
            # correct = np.squeeze(correct_tensor.cpu().numpy())
            # train_num_correct += np.sum(correct)
            
            with warmup_scheduler.dampening():
                scheduler.step()

            preds = F.softmax(logits,dim=1).argmax(dim=1)
            predictions.append(preds.detach().cpu().numpy())
            true_labels.extend(labels.to('cpu').numpy())
        predict_all = np.vstack(predictions)
        label_all = np.stack(true_labels)

        train_acc = metrics.accuracy_score(predict_all, label_all)
        avg_train_loss = total_train_loss / total_train_len

        # logging.info("###############################")
        # logging.info("Running Validation...")
        # logging.info("###############################")
        print("###############################")
        print("Running Validation...")
        print("###############################")
        total_val_loss, total_val_len = 0, 0

        model.eval()
        predictions, true_labels = [], []

        for _, batch in enumerate(tqdm(dev_dataloader, desc="Iteration")):

            image, texts, labels, targets = batch

            total_val_len += image.shape[0]
            image = image.to(opt.device)
            labels = labels.to(opt.device)
            targets = targets.to(opt.device)
            with torch.no_grad():
                logits = model(image, texts)

            loss = crit(logits, targets)
            total_val_loss += loss.item()

            # pred = predict_out.argmax(1, keepdim=True).float()
            # correct_tensor = pred.eq(labels.float().view_as(pred))
            # correct = np.squeeze(correct_tensor.cpu().numpy())
            # val_num_correct += np.sum(correct)
            preds = F.softmax(logits,dim=1).argmax(dim=1)
            predictions.append(preds.detach().cpu().numpy())
            true_labels.extend(labels.to('cpu').numpy())
        predict_all = np.vstack(predictions)
        label_all = np.stack(true_labels)

        val_acc = metrics.accuracy_score(predict_all, label_all)
        # val_acc = val_num_correct / total_val_len
        avg_val_loss = total_val_loss / total_val_len

        print()
        # logging.info("Epoch | Train Accuracy  | label_0 F1  | label_1 F1  | label_2 F1  | label_3 F1  | label_4 F1  | Validation F1  | Training Loss | Validation Loss")
        # logging.info(
        #     f"{train_idx + 1:4d} |  {train_acc:.4f}  |    {f_score[0]:.4f}     |    {f_score[1]:.4f}      |    {f_score[2]:.4f}     |    {f_score[3]:.4f}    |    {f_score[4]:.4f}    |   {val_f1:.4f}      |    {avg_train_loss:.4f}    |     {avg_val_loss:.4f}")
        print(
            "Epoch | Train Accuracy  | Val Accuracy | Training Loss | Validation Loss")
        print(
            f"{epoch + 1:4d} |  {train_acc:.4f}  |  {val_acc:.4f}  |  {avg_train_loss:.4f}  |   {avg_val_loss:.4f}")
        
        # print(
        #         f'''Epochs: {epoch + 1} 
        #       | Train Loss: {total_train_loss / len(train_data): .3f} 
        #       | Val Loss: {total_val_loss / len(val_data): .3f} ''')
        
        # save the best model
        model_save_path = os.path.join(opt.save_path)
        if not os.path.exists(model_save_path):
            os.makedirs(model_save_path)

        if val_acc > best_val_score:
            torch.save(model.state_dict(), os.path.join(model_save_path, 'model_best.pth'))
            best_val_score = val_acc
            # logging.info("**********best val************")

