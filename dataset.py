import torch
import torchvision
import pandas as pd
import json
from pathlib import Path
import numpy as np
import pandas as pd
from PIL import Image
import cv2 
import os
import easyocr
import math

class HatefulMemesDataset(torch.utils.data.Dataset):
    """Uses jsonl data to preprocess and serve 
    dictionary of multimodal tensors for model input.
    """
    def __init__(self, data_path, image_path, balance=False, dev_limit=None, random_state=0):     
        # read data
        self.samples_frame = pd.read_json(data_path, lines=True)
        self.data_path = data_path
        self.image_path = image_path
        self.dev_limit = dev_limit
        if balance:
            neg = self.samples_frame[self.samples_frame.label.eq(0)]
            pos = self.samples_frame[self.samples_frame.label.eq(1)]
            self.samples_frame = pd.concat(
                [
                    neg.sample(
                        pos.shape[0], 
                        random_state=random_state
                    ), 
                    pos
                ]
            )
        if dev_limit:
            if self.samples_frame.shape[0] > dev_limit:
                self.samples_frame = self.samples_frame.sample(dev_limit, random_state=random_state)
        self.samples_frame = self.samples_frame.reset_index(drop=True)
        self.reader =  easyocr.Reader(['en'])
        self.img_dim = 224
        self.text = [
                    json.loads(line)["text"] + '/n'
                    for line in open(
                        data_path
                    ).read().splitlines()
                ]
    
    def midpoint(self, x1, y1, x2, y2):
        x_mid = int((x1 + x2)/2)
        y_mid = int((y1 + y2)/2)
        return (x_mid, y_mid)

    def image_transform(self, image_path, ocr):
        '''
        Image Cleaning: easyocr.readtext and cv2.inpainting
        '''
        # read image
        img = cv2.imread(image_path)
        if len(img.shape) == 2:
            img = np.repeat(img[:, :, np.newaxis], 3, axis=2)

        result = ocr.readtext(image_path, paragraph = True)
        mask = np.zeros(img.shape[:2], dtype="uint8")

        for (box, _) in result:
            (tl, tr, br, bl) = box
            x0, y0 = (int(tl[0]), int(tl[1]))
            x1, y1 = (int(br[0]), int(br[1])) 
            x2, y2 = (int(tr[0]), int(tr[1]))
            x3, y3 = (int(bl[0]), int(bl[1])) 
 
            x_mid0, y_mid0 = self.midpoint(x1, y1, x2, y2)
            x_mid1, y_mi1 = self.midpoint(x0, y0, x3, y3)
 
            thickness = int(math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 ))
 
            cv2.line(mask, (x_mid0, y_mid0), (x_mid1, y_mi1), 255, thickness)
            img = cv2.inpaint(img, mask, 7, cv2.INPAINT_NS)

        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        img = Image.fromarray(img)
        image_transform = torchvision.transforms.Compose(
            [
                torchvision.transforms.Resize(
                    size=((224,224))
                ),        
                torchvision.transforms.ToTensor(),
            ]
        )
        img = image_transform(img)
        return img
    
    def __len__(self):
        return len(self.samples_frame)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        # img_id = self.samples_frame.loc[idx, "id"]
        img_path = os.path.join(str(self.image_path)[:-3], self.samples_frame.loc[idx, 'img'])
        image = self.image_transform(img_path, self.reader) # image cleaning
        text = self.text[idx]
        
        if "label" in self.samples_frame.columns:
            label=torch.tensor(self.samples_frame.loc[idx, "label"])
            target=torch.from_numpy(np.zeros((2),dtype=np.float32))
            target[label]=1.0
            
            batch = (image, text, label, target) 
        else:
            batch = (image, text)
        return batch
