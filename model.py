from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import torch
import torch.nn as nn
import torchvision
from torch.autograd import *
from modules import masked_mean, MyMultiHeadAttention
from transformers import BertModel, BertConfig, BertTokenizer
from transformers import AutoImageProcessor, BeitModel
from collections import OrderedDict
from torch.nn.utils.weight_norm import weight_norm

class MultimodalEncoder(nn.Module):
    def __init__(self, opt):
        """Initialize model."""
        super(MultimodalEncoder, self).__init__()
        self.opt = opt
        self.bert_hidden_size = opt.bert_hidden_size # 768
        self.img_feat_size = opt.img_feat_size # 4096
        self.beit_hidden_size = opt.beit_hidden_size # 768

        self.dropout = nn.Dropout(opt.dropout)

        # initialize task_specific classifier
        layers=[
            nn.Dropout(opt.dropout,inplace=True),
            weight_norm(nn.Linear(opt.bert_hidden_size * 3,opt.bert_hidden_size),dim=None),
            nn.ReLU(),
            weight_norm(nn.Linear(opt.bert_hidden_size,opt.trg_class),dim=None)
        ]
        self.classifer = nn.Sequential(*layers)

        # initialize multihead attetion layer
        n_head, d_kv, stack_num = 6, 128, 4
        self.text2img_multi_attention = nn.ModuleList(
            [MyMultiHeadAttention(n_head, self.bert_hidden_size, d_kv, dropout=opt.dropout, need_mask=False)
            for _ in range(stack_num)])
        self.text2vgg_multi_attention = nn.ModuleList(
            [MyMultiHeadAttention(n_head, self.bert_hidden_size, d_kv, dropout=opt.dropout, need_mask=False)
            for _ in range(stack_num)])
        
        self.proj_img2att = nn.Linear(self.beit_hidden_size, opt.bert_hidden_size)
        self.proj_vgg2att = nn.Linear(self.img_feat_size, opt.bert_hidden_size)
        # self.proj_img2fc = nn.Linear(self.fc_feat_size, opt.bert_hidden_size)
        # self.proj_vgg2fc = nn.Linear(self.fc_feat_size, opt.bert_hidden_size)
        # self.linear_cap_att = nn.Linear(self.bert_hidden_size, opt.bert_hidden_size)
        self.proj_text2fc = nn.Linear(self.bert_hidden_size, opt.bert_hidden_size)
        # self.linear_text2cap_fc = nn.Linear(self.bert_hidden_size, opt.bert_hidden_size)
        self.proj_text2img_attn = nn.Linear(self.bert_hidden_size, opt.bert_hidden_size)
        self.proj_text2vgg_attn = nn.Linear(self.bert_hidden_size, opt.bert_hidden_size)

        bert_version = "bert-base-cased"
        config = BertConfig.from_pretrained(bert_version)
        self.bert_model = BertModel.from_pretrained(bert_version, config=config)
        
        # self.vgg_net = torch.load('vgg19-fine-tuned.pt', map_location='cpu')
        vgg = torchvision.models.vgg19()
        self.vgg_net = self.load_model(vgg, 'vgg19-fine-tuned.pt')
        new_classifier = torch.nn.Sequential(*list(self.vgg_net.children())[-1][:6])
        self.vgg_net.classifier = new_classifier
        
        beit_version = 'microsoft/beit-base-patch16-224-pt22k-ft22k'
        self.processor = AutoImageProcessor.from_pretrained(beit_version)
        self.beit_model = BeitModel.from_pretrained(beit_version)
    
    def encode_text(self, texts):
        batch_texts = [text for text in texts]
        tokenizer = BertTokenizer.from_pretrained('bert-base-cased')
        input = tokenizer(batch_texts,
                         padding='max_length', 
                         max_length = 100, 
                         truncation=True,
                         return_tensors="pt").to(self.opt.device)
        attention_mask = list(input.values())[-1]
        seq_output = self.bert_model(**input)[0]  # [0] is the seq_output, [1] is the pooler_output
        text_feature = masked_mean(seq_output, mask=attention_mask)
        return text_feature
    
    def beit_encode_image(self, image):
        inputs = self.processor(images=image, return_tensors="pt").to(self.opt.device)
        # with torch.no_grad():
        outputs = self.beit_model(**inputs)[1]
        # image_feature = masked_mean(last_hidden_states)
        batch_size = outputs.size(0)
        feat_size = outputs.size(-1)
        outputs = outputs.view(batch_size, -1, feat_size)
        image_feature = outputs
        return image_feature

    def load_model(self, model, load_path, strict=False):
        load_net = torch.load(load_path, map_location=torch.device('cpu'))['model']
        load_net_clean = OrderedDict()
        for k, v in load_net.items():
            if k.startswith('vgg.'):
                load_net_clean[k[6:]] = v
            else:
                load_net_clean[k] = v
        model.load_state_dict(load_net_clean, strict=strict)
        return model

    def forward(self, image, text):
        image_feature = self.beit_encode_image(image)
        vgg_feature = self.vgg_net(image) # [batch, 4096]
        batch_size = vgg_feature.size(0)
        feat_size = vgg_feature.size(-1)
        vgg_feature = vgg_feature.view(batch_size, -1, feat_size)
        text_feature = self.encode_text(text)
        
        # re-scale image for multihead attention
        img_att_feature = self.proj_img2att(image_feature)
        vgg_att_feature = self.proj_vgg2att(vgg_feature)
        # re-scale text for classifier
        fc_text = self.proj_text2fc(text_feature)
        # align text and image for multihead attention
        att_text2img = self.proj_text2img_attn(text_feature)
        att_text2vgg = self.proj_text2vgg_attn(text_feature)
        
        # perform multimead attention
        for layer in self.text2vgg_multi_attention:
            att_text2vgg, _ = layer(att_text2vgg, vgg_att_feature, vgg_att_feature)

        for layer in self.text2img_multi_attention:
            att_text2img, _ = layer(att_text2img, img_att_feature, img_att_feature)

        predict = self.classifer(torch.cat((att_text2vgg, att_text2img, fc_text), dim=1))      
        return predict
    
