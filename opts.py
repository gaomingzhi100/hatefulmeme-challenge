import argparse
from pathlib import Path

data_dir = Path.cwd() / "hateful_memes"
img_path = data_dir / "img"
train_path = data_dir / "train.jsonl"
dev_path = data_dir / "dev_seen.jsonl"
dev_unseen_path = data_dir / "dev_unseen.jsonl"
test_path = data_dir / "test_seen.jsonl"

def parse_opt():

    parser = argparse.ArgumentParser()

    # Data input settings
    parser.add_argument('--img_path', type=str, default=img_path,
                    help='path to the json file containing the image dataset')
    parser.add_argument('--train_path', type=str, default=train_path,
                    help='path to the json file containing the train dataset')
    parser.add_argument('--dev_path', type=str, default=dev_path,
                    help='path to the json file containing the dev dataset')
    parser.add_argument('--dev_unseen_path', type=str, default=dev_unseen_path,
                    help='path to the json file containing the dev unseen dataset')
    parser.add_argument('--test_path', type=str, default=test_path,
                    help='path to the json file containing the testdataset')
    
    # Model settings
    parser.add_argument('--bert_hidden_size', type=int, default=768,
                        help='the hidden size of BERT')
    parser.add_argument('--img_feat_size', type=int, default=4096,
                        help='2048 for resnet, 4096 for vgg')
    parser.add_argument('--beit_hidden_size', type=int, default=768,
                        help='the hidden size of BEiT')
    # parser.add_argument('--bert_layer_num', type=int, default=6,
    #                     help='number of layers in the BERT')
    parser.add_argument('--trg_class', type=int, default=2,
                        help='num of target class')
    parser.add_argument('--save_path', type=str,
                        default='./saved_model',
                        help='path to save the (best) model')
    parser.add_argument('--output_path', type=str,
                        default='model-outputs',
                        help='')
    # parser.add_argument('--mode', type=str, default='multihead_text',
    #                     help='choose the second mode from [concat, attention, attention_text, multihead, multihead_text, co_att')
    

    # Optimization: General
    parser.add_argument('--warmup_proportion', type=float, default=0.1,
                        help='the ratio to warm up the training of the BERT')
    parser.add_argument('--seed', type=int, default=10,
                        help='random seed')
    parser.add_argument('--train_batch_size', type=int, default=10,
                    help='minibatch size')
    parser.add_argument('--num_workers', type=int, default=4,
                    help='number of workers in dataloader')
    parser.add_argument('--learning_rate', type=float, default=5e-5,
                        help='learning rate')
    parser.add_argument('--dropout', type=float, default=0.1,
                        help='learning rate')
    parser.add_argument('--max_epochs', type=int, default=10,
                    help='number of epochs')
    parser.add_argument('--checkpoint_monitor', type=str, default='avg_val_loss',
                    help='pl_checkpoint_monitor')
    
    
    args, unknown = parser.parse_known_args()
    return args


def hparams():
    hparams = {
        # Required hparams
    "train_path": train_path,
    "dev_path": dev_path,
    "img_dir": data_dir,
    
    # Optional hparams
    # "embedding_dim": 150,
    # "language_feature_dim": 512,
    # "vision_feature_dim": 512,
    # "fusion_output_size": 256,
    "output_path": "model-outputs",
    "dev_limit": None,
    "lr": 0.00005,
    "max_epochs": 10,
    "n_gpu": 0,
    "batch_size": 4,
    # allows us to "simulate" having larger batches 
    "accumulate_grad_batches": 6,
    "early_stop_patience": 3,
}
    return hparams
    
